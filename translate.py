# coding: utf-8
import argparse
from src.data import load_vocab
from src.model import get_model
from src.beam import translate
import os
import json
import numpy as np


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-no_cuda', action='store_true', default=False)
    parser.add_argument('-load_weights', default='training_19')
    parser.add_argument('-ckpt_file', default='best_model.ckpt')
    parser.add_argument('-k', type=int, default=2)
    parser.add_argument('-vocab_size', type=str, default=42)
    # parser.add_argument('-vocab_src', type=str, default='vocabulary.txt')
    parser.add_argument('-vocab_dump', type=str, default='vocab.json')
    parser.add_argument('-tied_layers', action='store_true', default=False)
    parser.add_argument('-absolute_positioning', action='store_true', default=True)
    parser.add_argument('-absolute_positioning_style', type=str, default='before') # before | after
    parser.add_argument('-absolute_positioning_encoding', type=str, default='original') # original | bqc
    parser.add_argument('-relative_positioning', action='store_true', default=True)
    parser.add_argument('-relative_positioning_style', type=str, default='one-way') # one-way | two-ways
    parser.add_argument('-fixed_positioning', action='store_true', default=True)
    parser.add_argument('-ff_activation', type=str, default='gelu') # relu | gelu | biprelu | maxout | offsetmaxout | clampedoffsetmaxout | dyrelub
    parser.add_argument('-n_encoder_layers', type=int, default=1)
    parser.add_argument('-n_decoder_layers', type=int, default=1)
    parser.add_argument('-d_vocab', type=int, default=4)
    parser.add_argument('-d_hidden', type=int, default=16)
    parser.add_argument('-heads', type=int, default=4)
    parser.add_argument('-d_ff', type=int, default=1*16)
    parser.add_argument('-dropout', type=int, default=0.0)
    parser.add_argument('-batchsize', type=int, default=1)
    parser.add_argument('-max_tokens', type=int, default=18)

    args = parser.parse_args()

    args.device = 0 if args.no_cuda is False else -1

    assert args.k > 0
    assert args.max_tokens > 1

    vocab_dump = os.path.join('data', args.vocab_dump)
    if os.path.exists(vocab_dump):
        args.pad_id = 0
        args.unk_id = 1
        args.sos_id = 2
        args.eos_id = 3
        args.mask_id = 4
        args = load_vocab(vocab_dump, args)
    else:
        print('No vocab file found at '+vocab_dump)
        quit()
    model = get_model(args)
    model.eval()
    # print('pos', model.ff.activation.pos.min(), model.ff.activation.pos.max())
    # print('neg', model.ff.activation.neg.min(), model.ff.activation.neg.max())
    # mat = np.vstack((model.ff.activation.pos.unsqueeze(0).cpu().detach().numpy(), model.ff.activation.neg.unsqueeze(0).cpu().detach().numpy()))
    # print('mat.shape', mat.shape)
    # print('mat[:, 0]', mat[:, 0])
    # mask1 = mat[0, :]>0.1
    # print('mask1', type(mask1), mask1.shape, mask1)
    # mask2 = mat[1, :]>-0.01
    # print('mask2', type(mask2), mask2.shape, mask2)
    # conj = mask1 & mask2
    # print('conj', type(conj), conj.shape, conj)
    # nb_relu = sum(conj)
    # print('relu:', nb_relu)
    # mask1 = mat[1, :]<-0.1
    # print('mask1', type(mask1), mask1.shape, mask1)
    # mask2 = mat[0, :]<0.01
    # print('mask2', type(mask2), mask2.shape, mask2)
    # conj = mask1 & mask2
    # print('conj', type(conj), conj.shape, conj)
    # nb_relu = sum(conj)
    # print('neg relu:', nb_relu)
    # mask1 = mat[0, :]>0.01
    # print('mask1', type(mask1), mask1.shape, mask1)
    # mask2 = mat[1, :]<-0.01
    # print('mask2', type(mask2), mask2.shape, mask2)
    # conj = mask1 & mask2
    # print('conj', type(conj), conj.shape, conj)
    # nb_xor = sum(conj)
    # print('neg relu:', nb_xor)
    # for i in range(len(model.ff.activation.pos)):
    #     print(i, '-', float(model.ff.activation.neg[i]), '-', float(model.ff.activation.pos[i]))

    while True:
        args.text = input("Enter a sentence to translate (type 'f' to load \
                           from file, or 'q' to quit):\n")
        if args.text == "q":
            break
        if args.text == 'f':
            input("Enter a sentence to translate (type 'f' to load \
                   from file, or 'q' to quit):\n")
            try:
                args.text = ' '.join(open(args.text, encoding='utf8')
                                     .read()
                                     .split('\n'))
            except:
                print("error opening or reading text file")
                continue
        phrase = ' '.join(translate(args, model))
        print('---------------------------------------------\n'+phrase+'\n')

if __name__ == '__main__':
    main()
