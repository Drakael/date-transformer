# coding: utf-8
import argparse
from src.data import process_data
from src.model import get_model
from src.trainer import train_model
import torch
import numpy as np
import os


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-src', type=str, default='data/dates_dataset_1760-2080.csv')
    parser.add_argument('-eval_ratio', type=float, default=0.1)
    parser.add_argument('-no_cuda', action='store_true', default=False)
    parser.add_argument('-SGDR', action='store_true', default=False)
    parser.add_argument('-epochs', type=int, default=400)
    parser.add_argument('-train_dir', type=str, default='trainings')
    parser.add_argument('-load_weights', type=str, default='trainings')
    parser.add_argument('-ckpt_file', type=str, default='model_weights.ckpt')
    # parser.add_argument('-ckpt_file', type=str, default='model_weights_e65_loss2.504946255683899_acc0.9367612123489379.ckpt')
    parser.add_argument('-vocab_size', type=int, default=42)
    parser.add_argument('-vocab_src', type=str, default='vocabulary.txt')
    parser.add_argument('-vocab_dump', type=str, default='vocab.json')
    parser.add_argument('-tied_layers', action='store_true', default=False)
    parser.add_argument('-absolute_positioning', action='store_true', default=False)
    parser.add_argument('-absolute_positioning_style', type=str, default='before') # before | after
    parser.add_argument('-absolute_positioning_encoding', type=str, default='original') # original | bqc
    parser.add_argument('-relative_positioning', action='store_true', default=False)
    parser.add_argument('-relative_positioning_style', type=str, default='two-ways') # one-way | two-ways
    parser.add_argument('-fixed_positioning', action='store_true', default=True)
    parser.add_argument('-rotary_positioning', action='store_true', default=True)
    parser.add_argument('-ff_activation', type=str, default='gelu') # relu | gelu | biprelu | maxout | offsetmaxout | clampedoffsetmaxout | dyrelub
    parser.add_argument('-n_encoder_layers', type=int, default=1)
    parser.add_argument('-n_decoder_layers', type=int, default=1)
    parser.add_argument('-d_vocab', type=int, default=4)
    parser.add_argument('-d_hidden', type=int, default=16)
    parser.add_argument('-heads', type=int, default=4)
    parser.add_argument('-d_ff', type=int, default=1*16)
    parser.add_argument('-batch_size', type=int, default=2048*4)
    parser.add_argument('-batch_cuts', type=str, default=1)
    parser.add_argument('-dropout', type=float, default=0.0)
    parser.add_argument('-label_smoothing', type=float, default=0.0)
    parser.add_argument('-printevery', type=int, default=1)
    parser.add_argument('-lr', type=float, default=2e-3)
    parser.add_argument('-patience', type=int, default=-1)
    parser.add_argument('-min_tokens', type=int, default=1)
    parser.add_argument('-max_tokens', type=int, default=64)
    parser.add_argument('-checkpoint', type=int, default=20)

    args = parser.parse_args()

    args.device = 0 if args.no_cuda is False else -1
    if args.device == 0:
        assert torch.cuda.is_available()
    print('Source :', str(args.src))
    print('Eval ratio :', str(args.eval_ratio))
    print('Device :', 'cuda' if args.device == 0 else 'cpu')
    print('Epochs :', str(args.epochs))
    print('Batch size :', str(args.batch_size))
    print('Batch cuts :', str(args.batch_cuts))
    print('Vocab embedding :', str(args.d_vocab))
    print('Model embedding :', str(args.d_hidden))
    print('Encoder Layers :', str(args.n_encoder_layers))
    print('Decoder Layers :', str(args.n_decoder_layers))
    print('Heads :', str(args.heads))
    print('FF size :', str(args.d_ff))
    print('Tied layers :', str(args.tied_layers))
    print('Absolute positioning :', str(args.absolute_positioning))
    print('Absolute positioning style :', str(args.absolute_positioning_style))
    print('Absolute positioning encoding :', str(args.absolute_positioning_encoding))
    print('Relative positioning :', str(args.relative_positioning))
    print('Relative positioning style:', str(args.relative_positioning_style))
    print('Fixed positioning :', str(args.fixed_positioning))
    print('Rotary positioning :', str(args.rotary_positioning))
    print('FF activation :', str(args.ff_activation))
    print('Min tokens :', str(args.min_tokens))
    print('Max tokens :', str(args.max_tokens))
    print('Label smoothing :', str(args.label_smoothing))
    print('Dropout :', str(args.dropout))
    print('Learning rate :', str(args.lr))
    print('Patience :', str(args.patience))

    args.train_iterator, args.eval_iterator, args = process_data(args)
    args.model = get_model(args)

    total_trainable_params = sum(p.numel() for p in args.model.parameters()
                                 if p.requires_grad)
    total_params = sum(p.numel() for p in args.model.parameters())
    print('Number of trainable parameters :', total_trainable_params,
          '/', total_params)

    batch_count = 0
    count = 0
    src_pad_count = 0
    src_tok_count = 0
    trg_pad_count = 0
    trg_tok_count = 0
    for i, (src, trg, src_counts, start, end) in enumerate(args.train_iterator):
        batch_count += 1
        for s, t in zip(src, trg):
            for tok in s.cpu().numpy().tolist():
                src_tok_count += 1
                if tok == args.pad_id:
                    src_pad_count += 1
            for tok in t.cpu().numpy().tolist():
                trg_tok_count += 1
                if tok == args.pad_id:
                    trg_pad_count += 1
            count += 1
    print('Number of examples iterated :', count)
    print('Batch count :', batch_count)
    print('Src waste (pad tokens) :', str(src_pad_count), '/',
          str(src_tok_count), '=',
          str(round(src_pad_count / src_tok_count * 100, 1)), '%')
    print('Trg waste (pad tokens) :', str(trg_pad_count), '/',
          str(trg_tok_count), '=',
          str(round(trg_pad_count / trg_tok_count * 100, 1)), '%')

    index = 0
    while os.path.exists(os.path.join(args.train_dir, 'training_'+str(index))):
        index += 1
    args.training_dir = os.path.join(args.train_dir, 'training_'+str(index))
    os.mkdir(args.training_dir)
    train_model(args)

if __name__ == "__main__":
    cur_seed = 1
    np.random.seed(cur_seed)
    torch.manual_seed(cur_seed)
    torch.cuda.manual_seed_all(cur_seed)
    main()
