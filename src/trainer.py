# coding: utf-8
import torch
from time import time
import os
import numpy as np
from src.model import create_masks
from src.optim import CosineWithRestarts, ls_loss
import json
from torch.optim.lr_scheduler import ReduceLROnPlateau
from sklearn.model_selection import StratifiedKFold


def train_init(args):
    print("training model...")
    model = args.model
    model.train()
    args.optimizer = torch.optim.Adam(model.parameters(), lr=args.lr,
                                      betas=(0.9, 0.98), eps=1e-9,
                                      amsgrad=False)
    # args.optimizer = torch.optim.Adamax(model.parameters(), lr=0.002,
    #                                     betas=(0.9, 0.999), eps=1e-8,
    #                                     weight_decay=0)
    if args.patience > 0:
        args.scheduler = ReduceLROnPlateau(args.optimizer, patience=args.patience,
                                           verbose=True)

    if args.SGDR is True:
        args.sched = CosineWithRestarts(args.optimizer, T_max=args.train_len)
        import matplotlib.pyplot as plt
        plt.plot(np.arange(1, 20000), [args.sched.get_lr()
                                       for i in range(1, 20000)])
        plt.show()

    if args.checkpoint > 0:
        print("model weights will be saved every {:d} epochs "
              "to directory {}/".format(args.checkpoint,
                                        args.training_dir))

    if args.train_dir is not None:
        if not os.path.isdir(args.train_dir):
            os.mkdir(args.train_dir)
    return model


def training_loop(model, history, folder, args):
    t0 = time()
    best_eval_accuracy = 0
    best_eval_loss = 1e9
    best_epoch = None
    for epoch in range(args.epochs):
        avg_loss = None
        avg_accuracy = None
        total_loss = 0.0
        total_accuracy = 0.0
        loss_seq = list()
        acc_seq = list()
        for i, (src, trg, src_counts, start, end) in enumerate(args.train_iterator):
            # print('src_counts', src_counts)
            # print('src', src.size(), src)
            # print('trg', trg.size(), trg)
            # input doesn't take <eos> token
            trg_input = trg[:, :-1]
            # output doesn't take <sos> token
            gold = trg[:, 1:]
            src_mask, trg_mask = create_masks(src, trg_input, args)
            preds = model(src, trg_input, src_mask, trg_mask)
            preds_labels = preds.argmax(dim=-1)
            pad_mask = gold != args.pad_id
            nb_correct = (preds_labels[pad_mask] == gold[pad_mask]).float().sum()
            accuracy = nb_correct / (gold[pad_mask].view(-1).size(0))
            if (i % 150) == 0:
                rand_sample = np.random.randint(src.size(0))
                print('src[rand_sample, :]', src[rand_sample, :].dtype, src[rand_sample, :])
                print('trg_input[rand_sample, :]', trg_input[rand_sample, :].dtype, trg_input[rand_sample, :])
                print('gold[rand_sample, :]', gold[rand_sample, :].dtype, gold[rand_sample, :])
                print('preds_labels[rand_sample, :]', preds_labels[rand_sample, :].dtype, preds_labels[rand_sample, :])
                p_src = [args.src_id2t[str(id_)] for id_ in src[rand_sample, :].cpu().detach().numpy().tolist()]
                p_gold = [args.src_id2t[str(id_)] for id_ in gold[rand_sample, :].cpu().detach().numpy().tolist()]
                p_pred_labels = [args.src_id2t[str(id_)] for id_ in preds_labels[rand_sample, :].cpu().detach().numpy().tolist()]
                print(p_src)
                print(p_gold)
                print(p_pred_labels)
                print(' ')
            total_accuracy += float(accuracy)
            acc_seq.append(float(accuracy))
            ys = gold.contiguous().view(-1)
            args.optimizer.zero_grad()
            loss = ls_loss(preds.contiguous().view(-1, preds.size(-1)),
                           ys, args)
            loss.backward()
            args.optimizer.step()
            if args.SGDR is True:
                args.sched.step()

            cur_loss = loss.item() / args.batch_size
            loss_seq.append(float(cur_loss))
            total_loss += float(cur_loss)

            if (i + 1) % args.printevery == 0:
                p = int(100 * (i + 1) / args.batch_count)
                avg_loss = round(total_loss / args.printevery, 4)
                avg_accuracy = round(total_accuracy / args.printevery, 4)
                print("%dm: epoch %d [%s%s]  %d%%  avg_loss = %.3f "
                      "avg_accuracy = %.3f " %
                      ((time() - t0)//60, epoch + 1, "".join('#'*(p//5)),
                       "".join(' '*(20-(p//5))), p, avg_loss, avg_accuracy),
                      end='\r')
                total_loss = 0
                total_accuracy = 0
        if args.patience > 0:
            args.scheduler.step(epoch)
        print(" "*110, end='\r')
        if args.checkpoint > 0 and (epoch + 1) % args.checkpoint == 0:
            torch.save(model.state_dict(),
                       '{}/model_weights_e{}_loss{}_acc{}.ckpt'.format(
                            folder, epoch + 1,
                            np.mean(loss_seq), np.mean(acc_seq)))
        print("%dm: epoch %d complete, avg_loss = %.03f "
              "avg_accuracy = %.3f " %
              ((time() - t0)//60, epoch + 1, np.mean(loss_seq), np.mean(acc_seq)))
        history['loss'].append(np.mean(loss_seq))
        history['accuracy'].append(np.mean(acc_seq))
        history['runtime'].append((time() - t0)//60)
        with open(os.path.join(folder, 'history.json'), 'w') as f:
            json.dump(history, f)
        if hasattr(args, 'eval_iterator'):
            model.eval()
            avg_accuracy = None
            total_accuracy = 0.0
            acc_seq = list()
            # print('Evaluating model...')
            for i, (src, trg, src_counts, start, end) in enumerate(args.eval_iterator):
                # input doesn't take <eos> token
                trg_input = trg[:, :-1]
                # output doesn't take <sos> token
                gold = trg[:, 1:]
                src_mask, trg_mask = create_masks(src, trg_input, args)
                preds = model(src, trg_input, src_mask, trg_mask)
                preds_labels = preds.argmax(dim=-1)
                pad_mask = gold != args.pad_id
                nb_correct = (preds_labels[pad_mask] == gold[pad_mask]).float().sum()
                accuracy = nb_correct / gold[pad_mask].view(-1).size(0)
                total_accuracy += float(accuracy)
                acc_seq.append(float(accuracy))
                ys = gold.contiguous().view(-1)
                args.optimizer.zero_grad()
                loss = ls_loss(preds.contiguous().view(-1, preds.size(-1)),
                               ys, args)
                cur_loss = loss.item() / args.batch_size
                loss_seq.append(float(cur_loss))
            eval_accuracy = np.mean(acc_seq)
            eval_loss = np.mean(loss_seq)
            print('Eval loss :', round(eval_loss, 2), 'Eval accuracy :', round(eval_accuracy, 2))
            if best_eval_loss > eval_loss:
                best_eval_loss = eval_loss
                best_eval_accuracy = eval_accuracy
                best_epoch = epoch
                torch.save(model.state_dict(),
                           os.path.join(folder, 'best_model.ckpt'))
        # print('pos', model.ff.activation.maxout.pos.min(), model.ff.activation.maxout.pos.max())
        # print('neg', model.ff.activation.maxout.neg.min(), model.ff.activation.maxout.neg.max())
        # print('pos', model.ff.activation.pos.min(), model.ff.activation.pos.max())
        # print('neg', model.ff.activation.neg.min(), model.ff.activation.neg.max())

    torch.save(model.state_dict(),
               os.path.join(folder, 'model_weights.ckpt'))

def train_model(args):
    model = train_init(args)

    history = dict()
    history['settings'] = {
        'src': args.src,
        'batch_size': int(args.batch_size),
        'batch_cuts': args.batch_cuts,
        'd_vocab': args.d_vocab,
        'd_hidden': args.d_hidden,
        'd_ff': args.d_ff,
        'n_encoder_layers': args.n_encoder_layers,
        'n_decoder_layers': args.n_decoder_layers,
        'tied_layers': args.tied_layers,
        'absolute_positioning': args.absolute_positioning,
        'absolute_positioning_style': args.absolute_positioning_style,
        'absolute_positioning_encoding': args.absolute_positioning_encoding,
        'relative_positioning': args.relative_positioning,
        'relative_positioning_style': args.relative_positioning_style,
        'fixed_positioning': args.fixed_positioning,
        'rotary_positioning': args.rotary_positioning,
        'ff_activation': args.ff_activation,
        'heads': args.heads,
        'vocab_size': int(args.vocab_size),
        'min_tokens': int(args.min_tokens),
        'max_tokens': int(args.max_tokens),
        'dropout': args.dropout,
        'label_smoothing': args.label_smoothing,
        'lr': args.lr,
        'patience': int(args.patience),
        'epochs': int(args.epochs),
        'checkpoint': int(args.checkpoint),
        'printevery': int(args.printevery),
        'SGDR': args.SGDR,
        'no_cuda': args.no_cuda
    }
    history['loss'] = list()
    history['accuracy'] = list()
    history['runtime'] = list()
    training_loop(model, history, args.training_dir, args)
