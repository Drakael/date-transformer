# coding: utf-8
import torch
from src.data import tokenize_src
from src.model import create_masks, nopeak_mask
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
import math
import re
import os
from time import time
import sentencepiece as spm


def init_vars(src, model, args):
    # print('init src', src)
    init_tok = args.sos_id
    outputs = torch.LongTensor([[init_tok]]).to(args.device)
    # src_mask = (src != args.pad_id).unsqueeze(-2)
    src_mask, trg_mask = create_masks(src, outputs, args)
    e_output = model.encoder(src, src_mask)

    # print('init outputs in', outputs)

    # trg_mask = nopeak_mask(1, args)

    out = model.decoder(outputs, e_output, src_mask, trg_mask)
    out = model.to_vocab(out)
    out = model.to_token(out)
    # out = F.linear(out, model.embed.hidden_embed.weight.data.t())
    # out = F.linear(out, model.embed.vocab_embed.weight.data)
    # print('out', out.size())
    out = F.softmax(out, dim=-1)

    probs, ix = out[:, -1].data.topk(args.k)
    # print('init probs', probs)
    # print('init ix', ix)
    log_scores = torch.Tensor([math.log(p)
                               for p in probs.data[0]]).unsqueeze(0)

    outputs = torch.zeros(args.k, args.max_tokens).long().to(args.device)
    outputs[:, 0] = init_tok
    outputs[:, 1] = ix[0]
    # outputs[0, 1] = ix[0][1]
    # print('init outputs', outputs)

    e_outputs = torch.zeros(args.k, e_output.size(-2), e_output.size(-1)).to(args.device)
    e_outputs[:, :] = e_output[0]

    return outputs, e_outputs, log_scores


def k_best_outputs(outputs, out, log_scores, i, k):
    probs, ix = out[:, -1].data.topk(k)
    # print('probs', probs)
    # print('ix', ix)
    log_probs = torch.Tensor([math.log(p) for p in
                              probs.data.view(-1)]).view(k, -1)
    log_probs += log_scores.transpose(0, 1)
    k_probs, k_ix = log_probs.view(-1).topk(k)
    # print('k_probs', k_probs)
    # print('k_ix', k_ix)

    row = torch.div(k_ix, k, rounding_mode='floor')
    col = k_ix % k
    # print('row', row, 'col', col)

    outputs[:, :i] = outputs[row, :i]
    outputs[:, i] = ix[row, col]

    log_scores = k_probs.unsqueeze(0)

    # print('outputs', outputs)
    # print('log_scores', log_scores)

    return outputs, log_scores


def beam_search(src, model, args):
    t0 = time()
    outputs, e_outputs, log_scores = init_vars(src, model, args)
    # print('outputs post init', outputs)
    eos_tok = args.eos_id
    # src_mask = (src != args.pad_id).unsqueeze(-2)
    ind = None
    for i in range(2, args.max_tokens):
        src_mask, trg_mask = create_masks(src, outputs[:, :i], args)
        # trg_mask = nopeak_mask(i, args)
        # print('outputs[:, :i]', i, outputs[:, :i])
        out = model.decoder(outputs[:, :i], e_outputs, src_mask, trg_mask)
        out = model.to_vocab(out)
        out = model.to_token(out)
        # out = F.linear(out, model.embed.hidden_embed.weight.data.t())
        # out = F.linear(out, model.embed.vocab_embed.weight.data)
        out = F.softmax(out, dim=-1)
        # print('beam out', out.size())

        outputs, log_scores = k_best_outputs(outputs, out, log_scores,
                                             i, args.k)
        # print('beam outputs', outputs)
        # Occurrences of end symbols for all input sentences.
        ones = (outputs == eos_tok).nonzero()
        sentence_lengths = torch.zeros(len(outputs), dtype=torch.long).to(args.device)
        for vec in ones:
            i = vec[0]
            # First end symbol has not been found yet
            if sentence_lengths[i] == 0:
                # Position of first end symbol
                sentence_lengths[i] = vec[1]

        num_finished_sentences = len([s for s in sentence_lengths if s > 0])

        if num_finished_sentences == args.k:
            alpha = 0.9
            div = 1/(sentence_lengths.type_as(log_scores)**alpha)
            _, ind = torch.max(log_scores * div, 1)
            ind = ind.data[0]
            break
    if ind is not None:
        ind = int(ind.cpu().numpy())
    outputs = outputs.cpu().numpy()
    print('beam done in ', str(time() - t0))
    if ind is None:
        ind = 0
    eos_tokens = (outputs[ind] == eos_tok).nonzero()[0].tolist()
    if type(eos_tokens) is list and len(eos_tokens) == 0:
        length = len(outputs[ind])
    elif type(eos_tokens) is list:
        length = int(eos_tokens[0])
    else:
        length = int(eos_tokens)
    seq = [args.src_id2t[str(tok)]
           for tok in outputs[ind][1:length]]
    # print('seq', seq)
    return ''.join(seq)


def translate_sentence(sentence, model, args):
    model.eval()
    if args.relative_positioning is True:
        indexed = [args.sos_id]
    else:
        indexed = []
    sentence = tokenize_src([sentence], args, 'sentence')
    print('sentence', sentence)
    for tok in sentence[0]:
        t = args.src_t2id[tok]
        if t != args.pad_id:
            indexed.append(t)
    if args.relative_positioning is True:
        indexed.append(args.eos_id)
    print('indexed', indexed)
    sentence = Variable(torch.LongTensor([indexed]))
    if args.device == 0:
        sentence = sentence.cuda()
    sentence = beam_search(sentence, model, args)

    return sentence


def translate(args, model):
    translated = []
    if type(args.text) is str:
        sentence = args.text
        if len(sentence) > 0:
            translated.append(translate_sentence(sentence, model, args))
    elif type(args.text) is list and len(args.text) > 0 and type(args.text[0]) is str:
        sentences = args.text
        for sentence in sentences:
            if len(sentence) > 0:
                translated.append(translate_sentence(sentence, model, args))
    else:
        print('No text argument found')
        return
    return translated
