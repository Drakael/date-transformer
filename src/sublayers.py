# coding: utf-8
import torch
import torch.nn as nn
import math
from src.optim import Gelu, DyReLUB, BiPRelu, MaxOut, OffsetMaxOut, ClampedOffsetMaxOut, PartialMaxOut
import matplotlib.pyplot as plt


class Norm(nn.Module):
    def __init__(self, d_hidden, device, eps=1e-6):
        super().__init__()
        self.size = d_hidden
        # create two learnable parameters to calibrate normalisation
        self.alpha = nn.Parameter(torch.ones(self.size))
        self.bias = nn.Parameter(torch.zeros(self.size))
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.alpha * (x - mean) / (std + self.eps) + self.bias


def attention(q, k, v, d_k, rpr, rpe_style, mask=None, dropout=None):
    # q, k and v dimensions are batch_size ; nb_heads ; seq_len ; d_heads
    scores = torch.matmul(q, k.transpose(-2, -1))
    # scores dimensions are batch_size ; nb_heads ; q_len ; k_len
    if rpr is None:
        scores /= math.sqrt(d_k)
    else:
        bs = q.size(0)
        h = q.size(1)
        ql = q.size(2)
        kl = k.size(2)
        q2 = q.permute(2, 0, 1, 3).reshape(ql, bs * h, d_k)
        # q2 dimensions are q_len ; batch_size * nb_heads ; d_heads
        # rpr dimensions are q_len ; k_len ; d_heads
        qp = torch.matmul(q2, rpr[:ql, :kl, :].transpose(-2, -1))
        # qp dimensions are q_len ; batch_size * nb_heads ; k_len
        qp = qp.view(ql, bs, h, kl).permute(1, 2, 0, 3)
        # qp dimensions are batch_size ; nb_heads ; q_len ; k_len
        scores += qp
        if rpe_style == 'two-ways':
            k2 = k.permute(2, 0, 1, 3).reshape(kl, bs * h, d_k)
            # k2 dimensions are k_len ; batch_size * nb_heads ; d_heads
            # rpr dimensions are k_len ; q_len ; d_heads
            kp = torch.matmul(rpr[:kl, :ql, :], k2.transpose(-2, -1))
            # kp dimensions are k_len ; batch_size * nb_heads ; q_len
            kp = kp.view(kl, bs, h, ql).permute(1, 2, 3, 0)
            # kp dimensions are batch_size ; nb_heads ; q_len ; k_len
            scores += kp
            scores /= math.sqrt(3*d_k)
        else:
            scores /= math.sqrt(2*d_k)
    if mask is not None:
        # mask.size is batch_size * 1 * seq_len
        mask = mask.unsqueeze(1)
        scores = scores.masked_fill(mask == 0, -1e12)
    scores = nn.Softmax(dim=-1)(scores)
    if dropout is not None:
        scores = dropout(scores)
    # scores dimensions are batch_size * nb_heads * q_len * k_len
    output = torch.matmul(scores, v)
    # output dimensions are batch_size * nb_heads * k_len * d_heads
    return output, scores


class MultiHeadAttention(nn.Module):
    def __init__(self, heads, d_hidden, rpe, rpe_style,
                 fixed, max_seq_len, device):
        super().__init__()
        self.d_hidden = d_hidden
        self.d_k = d_hidden // heads
        self.h = heads
        self.linear_q = nn.Linear(d_hidden, d_hidden, bias=True)
        self.linear_k = nn.Linear(d_hidden, d_hidden, bias=True)
        self.linear_v = nn.Linear(d_hidden, d_hidden, bias=True)
        self.scores = None
        self.dropout = None  # nn.Dropout(dropout)
        self.out = nn.Linear(d_hidden, d_hidden, bias=True)
        self.rpe_style = rpe_style
        if rpe is False:
            self.rpr = None
        else:
            if fixed is False:
                self.rpr = nn.Parameter(torch.zeros(max_seq_len,
                                                    max_seq_len,
                                                    self.d_k))
            else:
                self.rpr = torch.zeros((max_seq_len, max_seq_len, self.d_k),
                                       dtype=torch.float32).to(device)
                pe = torch.zeros((2 * max_seq_len + 1, self.d_k),
                                 dtype=torch.float32).to(device)
                pe.require_grad = False
                position = torch.arange(-max_seq_len - 1, max_seq_len,
                                        dtype=torch.float32).unsqueeze(1)
                # div_term = torch.arange(0, self.d_k, 2).float()
                # div_term = (-4.0 * (div_term / self.d_k)).exp()
                div_term = torch.arange(0, self.d_k // 2, 1).float()
                div_term = 2.0**div_term
                pi_2 = math.pi * 0.5
                pe[:, 0::2] = torch.sin(pi_2 * position / div_term)
                pe[:, 1::2] = torch.cos(pi_2 * position / div_term)
                # display relative positions layer
                # plt.figure()
                # plt.imshow(pe.cpu().numpy(), cmap='coolwarm', interpolation='nearest')
                # plt.show()
                # chunk relative positions layer into 3D matrix in the fomat [from; to; enc]
                for i in range(max_seq_len):
                    start = max_seq_len + 1 - i
                    end = start + max_seq_len
                    chunk = pe[start:end, :]
                    self.rpr[i, :, :] = chunk
                del pe

    def forward(self, q, k, v, mask=None, rot_pos=None):
        bs = q.size(0)
        k = k.long()
        # dimensions are batch_size * seq_len * d_hidden
        # perform linear operation and split into N heads
        q = self.linear_q(q.float()).view(bs, -1, self.h, self.d_k)
        k = self.linear_k(k.float()).view(bs, -1, self.h, self.d_k)
        v = self.linear_v(v.float()).view(bs, -1, self.h, self.d_k)
        # dimensions are batch_size * seq_len * nb_head * d_heads
        # transpose to get dimensions bs * N * sl * d_hidden
        q = q.transpose(1, 2)
        k = k.transpose(1, 2)
        v = v.transpose(1, 2)
        # dimensions are batch_size * nb_head * seq_len * d_heads
        if rot_pos is not None:
            q, k, v = self.apply_rotary_position_embeddings(
                rot_pos, q, k, v
            )
        # calculate attention using function we will define next
        output, self.scores = attention(q, k, v, self.d_k,
                                        self.rpr, self.rpe_style,
                                        mask, self.dropout)
        # concatenate heads and put through final linear layer
        concat = output.transpose(1, 2).contiguous() \
            .view(bs, -1, self.d_hidden)
        # dimensions are batch_size * seq_len * d_hidden
        output = self.out(concat)
        return output

    @staticmethod
    def apply_rotary_position_embeddings(sinusoidal_pos, q, k, v=None):
        # https://kexue.fm/archives/8265
        # sin [batch_size, num_heads, sequence_length, embed_size_per_head//2]
        # cos [batch_size, num_heads, sequence_length, embed_size_per_head//2]
        sin, cos = sinusoidal_pos.chunk(2, dim=-1)
        # sin [θ0,θ1,θ2......θd/2-1] -> sin_pos [θ0,θ0,θ1,θ1,θ2,θ2......θd/2-1,θd/2-1]
        sin_pos = torch.stack([sin, sin], dim=-1).reshape_as(sinusoidal_pos)
        # cos [θ0,θ1,θ2......θd/2-1] -> cos_pos [θ0,θ0,θ1,θ1,θ2,θ2......θd/2-1,θd/2-1]
        cos_pos = torch.stack([cos, cos], dim=-1).reshape_as(sinusoidal_pos)
        # rotate_half_query_layer [-q1,q0,-q3,q2......,-qd-1,qd-2]
        rotate_half_query_layer = torch.stack([-q[..., 1::2], q[..., ::2]], dim=-1).reshape_as(q)
        q = q * cos_pos + rotate_half_query_layer * sin_pos
        # rotate_half_key_layer [-k1,k0,-k3,k2......,-kd-1,kd-2]
        rotate_half_key_layer = torch.stack([-k[..., 1::2], k[..., ::2]], dim=-1).reshape_as(k)
        k = k * cos_pos[:, :, :k.size(2), :] + rotate_half_key_layer * sin_pos[:, :, :k.size(2), :]
        if v is not None:
            # rotate_half_value_layer [-v1,v0,-v3,v2......,-vd-1,vd-2]
            rotate_half_value_layer = torch.stack([-v[..., 1::2], v[..., ::2]], dim=-1).reshape_as(v)
            v = v * cos_pos[:, :, :k.size(2), :] + rotate_half_value_layer * sin_pos[:, :, :k.size(2), :]
            return q, k, v
        return q, k


class FeedForward(nn.Module):
    def __init__(self, d_hidden, d_ff=None, ff_activation='gelu'):
        super().__init__()
        if d_ff is None:
            # We set d_ff as a default to 4 * d_hidden
            d_ff = 4 * d_hidden
        self.d_ff = d_ff
        self.linear_1 = nn.Linear(d_hidden, d_ff, bias=True)
        self.linear_2 = nn.Linear(d_ff, d_hidden, bias=True)
        if ff_activation == 'relu':
            self.activation = nn.ReLU()
        elif ff_activation == 'gelu':
            self.activation = Gelu()
        elif ff_activation == 'biprelu':
            self.activation = BiPRelu(d_ff)
        elif ff_activation == 'maxout':
            self.activation = MaxOut(d_ff)
        elif ff_activation == 'offsetmaxout':
            self.activation = OffsetMaxOut(d_ff)
        elif ff_activation == 'clampedoffsetmaxout':
            self.activation = ClampedOffsetMaxOut(d_ff)
        elif ff_activation == 'dyrelub':
            self.activation = DyReLUB(d_ff)
        # self.dropout = nn.Dropout(dropout)
        # self.activation = OffsetMaxOut(d_ff)

    def forward(self, x):
        x = self.activation(self.linear_1(x))
        x = self.linear_2(x)
        return x
