# coding: utf-8
import torch
import torch.nn as nn
import math
import numpy as np
from torch.autograd import Variable
from torch.nn import Identity
from src.sublayers import Norm
import matplotlib.pyplot as plt
from typing import Optional


class Embedder(nn.Module):
    def __init__(self, vocab_size, d_vocab, d_hidden, max_tokens,
                 pad_id, device):
        super().__init__()
        # factorized embedding
        self.vocab_embed = nn.Embedding(vocab_size, d_vocab,
                                        padding_idx=pad_id)
        if d_vocab == d_hidden:
            self.hidden_embed = Identity()
        else:
            self.hidden_embed = nn.Linear(d_vocab, d_hidden, bias=True)
        self.norm = Norm(d_hidden, device)

    def forward(self, x):
        # factorized embedding
        e = self.vocab_embed(x)
        e = self.hidden_embed(e)
        return self.norm(e)


class PositionalEncoder(nn.Module):
    def __init__(self, d_hidden, fixed, device, encoding='original', max_seq_len=150):
        super().__init__()
        self.d_hidden = d_hidden
        # create constant 'pe' matrix with values dependant on
        # pos and i
        if fixed is False:
            pe = nn.Parameter(torch.zeros(max_seq_len, d_hidden,
                                          dtype=torch.float32))
        else:
            pe = torch.zeros(max_seq_len, d_hidden, dtype=torch.float32)
            pe.require_grad = False
            if encoding=='original':
                position = torch.arange(0, max_seq_len,
                                        dtype=torch.float32).unsqueeze(1)
                div_term = torch.arange(0, d_hidden, 2).float()
                div_term *= -(math.log(10000.0) / d_hidden)
                div_term = (div_term).exp()
                pe[:, 0::2] = torch.sin(position * div_term)
                pe[:, 1::2] = torch.cos(position * div_term)
            else:
                position = torch.arange(0, max_seq_len,
                                        dtype=torch.float32).unsqueeze(1)
                # div_term = torch.arange(0, self.d_k, 2).float()
                # div_term = (-4.0 * (div_term / self.d_k)).exp()
                div_term = torch.arange(0, d_hidden // 2).float()
                div_term = 2.0**div_term
                pi_2 = math.pi * 0.5
                pe[:, 0::2] = torch.sin(pi_2 * position / div_term)
                pe[:, 1::2] = torch.cos(pi_2 * position / div_term)
            # display absolute positions layer
            # plt.figure()
            # plt.imshow(pe.cpu().numpy(), cmap='coolwarm', interpolation='nearest')
            # plt.show()
            pe = pe.unsqueeze(0)
        self.pe = pe.to(device)
        # self.register_buffer('pe', pe)

    def forward(self, x):
        # make embeddings relatively larger
        x = x * math.sqrt(self.d_hidden)
        # add constant to embedding
        seq_len = x.size(1)
        pe = Variable(self.pe[:, :seq_len], requires_grad=False)
        # pe dimensions are 1 * seq_len * d_hidden
        x += pe
        return x

# Copied from transformers.models.marian.modeling_marian.MarianSinusoidalPositionalEmbedding with Marian->RoFormer
class RoFormerSinusoidalPositionalEmbedding(nn.Embedding):
    """This module produces sinusoidal positional embeddings of any length."""

    def __init__(self, num_positions: int, embedding_dim: int) -> None:
        super().__init__(num_positions, embedding_dim)
        self.weight = self._init_weight(self.weight)

    @staticmethod
    def _init_weight(out: nn.Parameter) -> nn.Parameter:
        """
        Identical to the XLM create_sinusoidal_embeddings except features are not interleaved. The cos features are in
        the 2nd half of the vector. [dim // 2:]
        """
        n_pos, dim = out.shape
        position_enc = np.array(
            [[pos / np.power(10000, 2 * (j // 2) / dim) for j in range(dim)] for pos in range(n_pos)]
        )
        out.requires_grad = False  # set early to avoid an error in pytorch-1.8+
        sentinel = dim // 2 if dim % 2 == 0 else (dim // 2) + 1
        out[:, 0:sentinel] = torch.FloatTensor(np.sin(position_enc[:, 0::2]))
        out[:, sentinel:] = torch.FloatTensor(np.cos(position_enc[:, 1::2]))
        out.detach_()
        return out

    @torch.no_grad()
    def forward(self, input_ids_shape: torch.Size) -> torch.Tensor:
        """`input_ids_shape` is expected to be [bsz x seqlen]."""
        bsz, seq_len = input_ids_shape[:2]
        positions = torch.arange(
            0, seq_len, dtype=torch.long, device=self.weight.device
        )
        return super().forward(positions)
