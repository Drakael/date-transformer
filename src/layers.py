# coding: utf-8
import torch.nn as nn
from src.sublayers import FeedForward, MultiHeadAttention, Norm


class EncoderLayer(nn.Module):
    def __init__(self, d_hidden, heads, d_ff, rpe, rpe_style, fixed,
                 ff_activation, max_seq_len, device):
        super().__init__()
        # self.norm = norm
        self.norm1 = Norm(d_hidden, device)
        self.norm2 = Norm(d_hidden, device)
        self.attn = MultiHeadAttention(heads, d_hidden, rpe, rpe_style,
                                       fixed, max_seq_len, device)
        self.ff = FeedForward(d_hidden, d_ff, ff_activation)
        # self.dropout = dropout_layer

    def forward(self, x, mask, rot_pos):
        x = self.norm1(x)
        # x = x + self.dropout(self.attn(x, x, x, mask))
        x = x + self.attn(x, x, x, mask, rot_pos)
        x = self.norm2(x)
        # x = x + self.dropout(self.ff(x))
        x = x + self.ff(x)
        return x


class DecoderLayer(nn.Module):
    def __init__(self, d_hidden, heads, d_ff, rpe, rpe_style, fixed,
                 ff_activation, max_seq_len, device):
        super().__init__()
        self.norm1 = Norm(d_hidden, device)
        self.norm2 = Norm(d_hidden, device)
        self.norm3 = Norm(d_hidden, device)
        # self.dropout = dropout_layer
        self.attn_1 = MultiHeadAttention(heads, d_hidden, rpe, rpe_style,
                                         fixed, max_seq_len, device)
        self.attn_2 = MultiHeadAttention(heads, d_hidden, rpe, rpe_style,
                                         fixed, max_seq_len, device)
        self.ff = FeedForward(d_hidden, d_ff, ff_activation)

    def forward(self, x, e_outputs, src_mask, trg_mask, rot_pos):
        # print('x', x.size())
        # print('e_outputs', e_outputs.size())
        x = self.norm1(x)
        # x = x + self.dropout(self.attn(x, x, x, trg_mask))
        x = x + self.attn_1(x, x, x, trg_mask, rot_pos)
        x = self.norm2(x)
        # x = x + self.dropout(self.attn(x, e_outputs, e_outputs,
        #                      src_mask))
        x = x + self.attn_2(x, e_outputs, e_outputs, src_mask, rot_pos)
        x = self.norm3(x)
        # x = x + self.dropout(self.ff(x))
        x = x + self.ff(x)
        return x
