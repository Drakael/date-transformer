# coding: utf-8
import torch
import numpy as np
import math
from torch import nn
import torch.nn.functional as F


def ls_loss(pred, gold, args):
    ''' Calculate cross entropy loss, apply label smoothing if needed. '''
    smoothing = args.label_smoothing
    vocab_size = args.src_vocab_size
    pad_id = args.pad_id
    gold = gold.contiguous().view(-1)
    pred = pred.contiguous().view(-1, pred.size(-1))

    if smoothing > 0.0:
        eps = smoothing
        n_class = pred.size(1)

        one_hot = torch.zeros_like(pred).scatter(1, gold.view(-1, 1), 1).to(args.device)
        one_hot = (one_hot * (1 - eps)) + ((1 - one_hot) * eps / (n_class - 1))
        log_prb = F.log_softmax(pred, dim=1)

        non_pad_mask = gold.ne(pad_id)
        loss = -(one_hot * log_prb).sum(dim=1)
        loss = loss.masked_select(non_pad_mask)
        loss = loss.sum()
    else:
        loss = F.cross_entropy(pred, gold,
                               reduction='sum',
                               ignore_index=pad_id)
    return loss


class Gelu(nn.Module):
    def forward(self, x):
        "Implementation of the gelu activation function by Hugging Face" 
        return x * 0.5 * (1.0 + torch.erf(x / math.sqrt(2.0)))


class CosineWithRestarts(torch.optim.lr_scheduler._LRScheduler):
    """
    Cosine annealing with restarts.

    Parameters
    ----------
    optimizer : torch.optim.Optimizer

    T_max : int
        The maximum number of iterations within the first cycle.

    eta_min : float, optional (default: 0)
        The minimum learning rate.

    last_epoch : int, optional (default: -1)
        The index of the last epoch.

    """
    def __init__(self,
                 optimizer: torch.optim.Optimizer,
                 T_max: int,
                 eta_min: float=0.,
                 last_epoch: int=-1,
                 factor: float=1.) -> None:
        self.T_max = T_max
        self.eta_min = eta_min
        self.factor = factor
        self._last_restart: int = 0
        self._cycle_counter: int = 0
        self._cycle_factor: float = 1.
        self._updated_cycle_len: int = T_max
        self._initialized: bool = False
        super(CosineWithRestarts, self).__init__(optimizer, last_epoch)

    def get_lr(self):
        """Get updated learning rate."""
        # HACK: We need to check if this is the first time get_lr() was called, since
        # we want to start with step = 0, but _LRScheduler calls get_lr with
        # last_epoch + 1 when initialized.
        if not self._initialized:
            self._initialized = True
            print('initialized')
            return self.base_lrs

        step = self.last_epoch + 1
        self._cycle_counter = step - self._last_restart

        lrs = [
            (
                self.eta_min + ((lr - self.eta_min) / 2) *
                (
                    np.cos(
                        np.pi *
                        ((self._cycle_counter) % self._updated_cycle_len) /
                        self._updated_cycle_len
                    ) + 1
                )
            ) for lr in self.base_lrs
        ]

        if self._cycle_counter % self._updated_cycle_len == 0:
            # Adjust the cycle length.
            self._cycle_factor *= self.factor
            self._cycle_counter = 0
            self._updated_cycle_len = int(self._cycle_factor * self.T_max)
            self._last_restart = step
        return lrs


class BiPRelu(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self.dim = dim
        self.neg = nn.Parameter(torch.ones(dim))
        self.pos = nn.Parameter(torch.zeros(dim))
        self.register_buffer('neg', torch.zeros(dim).float())
        self.register_buffer('pos', torch.ones(dim).float())

    def forward(self, x):
        mask = (x > 0)
        pos = x * self.pos
        neg = x * self.neg
        x[mask] = pos[mask]
        x[~mask] = neg[~mask]
        return x


# see https://arxiv.org/abs/1302.4389
class MaxOut(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self.pos = nn.Parameter(torch.ones(dim).fill_(0.2))
        self.neg = nn.Parameter(torch.ones(dim).fill_(-0.2))
        self.pos.requires_grad = True
        self.neg.requires_grad = True

    def forward(self, x):
        output_1 = (x * self.pos).unsqueeze(0)
        output_2 = (x * self.neg).unsqueeze(0)
        output = torch.cat((output_1, output_2), 0)
        output = torch.max(output, dim=0)[0]
        return output


class OffsetMaxOut(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self.pos = nn.Parameter(torch.ones(dim).fill_(0.2))
        self.neg = nn.Parameter(torch.ones(dim).fill_(-0.2))
        self.x0 = nn.Parameter(torch.zeros(dim))
        self.y0 = nn.Parameter(torch.zeros(dim))

    def forward(self, x):
        output_1 = (((x - self.x0) * self.pos) + self.y0).unsqueeze(0)
        output_2 = (((x - self.x0) * self.neg) + self.y0).unsqueeze(0)
        output = torch.cat((output_1, output_2), 0)
        output = torch.max(output, dim=0)[0]
        return output


class ClampedOffsetMaxOut(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self.pos = nn.Parameter(torch.ones(dim))
        self.neg = nn.Parameter(torch.ones(dim).fill_(0.1))
        self.x0 = nn.Parameter(torch.zeros(dim))
        self.y0 = nn.Parameter(torch.zeros(dim))
        self.max = nn.Parameter(torch.ones(dim))
        self.min = nn.Parameter(torch.zeros(dim))

    def forward(self, x):
        output_1 = (((x - self.x0) * self.pos) + self.y0).unsqueeze(0)
        output_2 = (((x - self.x0) * self.neg) + self.y0).unsqueeze(0)
        output = torch.cat((output_1, output_2), 0)
        output = torch.max(output, dim=0)[0]
        output = torch.max(torch.min(output, self.min), self.max)
        return output


class PartialMaxOut(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self.d_maxout = dim
        self.maxout = MaxOut(dim)

    def forward(self, x):
        output_1 = self.maxout(x[..., :self.d_maxout])
        output_2 = F.relu(x[..., self.d_maxout:])
        output = torch.cat((output_1, output_2), -1)
        return output
        

# Dynamic ReLU type B from https://arxiv.org/abs/2003.10027
class DyReLUB(nn.Module):
    def __init__(self, channels, reduction=2, k=2):
        super().__init__()
        self.channels = channels
        self.k = k
        self.fc1 = nn.Linear(channels, reduction)
        self.relu = nn.ReLU(inplace=True)
        self.fc2 = nn.Linear(reduction, 2*k*channels)
        self.sigmoid = nn.Sigmoid()

        self.register_buffer('lambdas', torch.Tensor([1.]*k + [0.5]*k).float())
        self.register_buffer('init_v', torch.Tensor([1.] + [0.]*(2*k - 1)).float())

    def get_relu_coefs(self, x):
        theta = torch.mean(x, axis=1)
        theta = self.fc1(theta)
        theta = self.relu(theta)
        theta = self.fc2(theta)
        theta = 2 * self.sigmoid(theta) - 1
        return theta

    def forward(self, x):
        theta = self.get_relu_coefs(x)
        relu_coefs = theta.view(-1, self.channels, 2*self.k) * self.lambdas + self.init_v
        # BxCxL -> LxBxCx1
        x_perm = x.permute(1, 0, 2).unsqueeze(-1)
        output = x_perm * relu_coefs[:, :, :self.k] + relu_coefs[:, :, self.k:]
        # LxBxCx2 -> BxCxL
        result = torch.max(output, dim=-1)[0].permute(1, 0, 2)
        return result
