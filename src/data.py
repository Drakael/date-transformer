# coding: utf-8
import numpy as np
import torch
from torch import from_numpy
import json
import os
import math
import matplotlib.pyplot as plt
from tqdm import tqdm
from random import shuffle


def tokenize_src(src, args, desc):
    tokens = []
    print('src[:10]', src[:10])
    for seq in tqdm(src, desc='Tokenizing '+desc):
        cur_seq = seq
        cur_seq_ids = []
        while len(cur_seq) > 0:
            for t in args.src_t2id.keys():
                t_len = len(t)
                if cur_seq[:t_len] == t:
                    cur_seq_ids.append(t)
                    cur_seq = cur_seq[t_len:]
        tokens.append(cur_seq_ids)
    return tokens


def load_vocab(vocab_path, args):
    with open(vocab_path, 'r') as f:
        vocab = json.load(f)
        args.src_t2id = vocab['src_t2id']
        args.src_id2t = vocab['src_id2t']
        args.token_count = vocab['token_count']
        args.total_tokens = vocab['total_tokens']
        args.src_vocab_size = len(args.src_id2t.keys())
        print('Loaded vocab from', vocab_path)
        print('Vocab size is', args.src_vocab_size)
        return args


def build_vocab(vocab_path, args, encoding='utf8'):
    args.src_t2id = {}
    args.src_t2id['<pad>'] = args.pad_id
    args.src_t2id['<unk>'] = args.unk_id
    args.src_t2id['<sos>'] = args.sos_id
    args.src_t2id['<eos>'] = args.eos_id
    args.src_t2id['<mask>'] = args.mask_id
    args.token_count = {}
    args.total_tokens = 0
    cnt = 5
    with open(vocab_path, 'r', encoding=encoding) as f:
        tokens = f.read().split('\n')
        for t in tokens:
            args.src_t2id[t] = cnt
            cnt += 1
            if t not in args.token_count.keys():
                args.token_count[t] = 1
            else:
                args.token_count[t] += 1
            args.total_tokens += 1
    args.src_id2t = {str(v): k for k, v in args.src_t2id.items()}
    args.src_vocab_size = len(args.src_id2t.keys())
    print('Vocab size is', args.src_vocab_size)
    vocab = {}
    vocab['src_t2id'] = args.src_t2id
    vocab['src_id2t'] = args.src_id2t
    vocab['token_count'] = args.token_count
    vocab['total_tokens'] = args.total_tokens
    vocab_dump = os.path.join('data', 'vocab.json')
    with open(vocab_dump, 'w') as f:
        json.dump(vocab, f)
    print('Saved vocab to', vocab_path)
    return args


def process_data(args, encoding='utf8'):
    args.pad_id = 0
    args.unk_id = 1
    args.sos_id = 2
    args.eos_id = 3
    args.mask_id = 4
    vocab_dump = os.path.join('data', args.vocab_dump)
    if os.path.exists(vocab_dump):
        args = load_vocab(vocab_dump, args)
    else:
        vocab_src = os.path.join('data', args.vocab_src)
        args = build_vocab(vocab_src, args)
    args.tokens_frequency = np.zeros((args.src_vocab_size,))
    for t, count in args.token_count.items():
        args.tokens_frequency[args.src_t2id[t]] = count
    args.tokens_frequency /= args.total_tokens
    print('Src vocab size :', args.src_vocab_size)
    print('Src total tokens :', args.total_tokens)
    # print(args.src_t2id)

    args.data = dict()
    args.data['src_str_list'] = list()
    args.data['src_tokens'] = list()
    args.data['trg_str_list'] = list()
    args.data['trg_tokens'] = list()
    if args.src is not None and os.path.exists(args.src):
        with open(args.src, 'r', encoding=encoding) as f:
            lines = f.read().strip().split('\n')
            shuffle(lines)
            for line in lines:
                split = line.split('\t')
                args.data['src_str_list'].append(split[0])
                args.data['trg_str_list'].append(split[1])

            args.data['src_tokens'] = tokenize_src(args.data['src_str_list'], args, 'source')
            args.data['trg_tokens'] = tokenize_src(args.data['trg_str_list'], args, 'target')
    # print('src tokens', args.data['src_tokens'][:10])
    # print('trg tokens', args.data['trg_tokens'][:10])

    args.train_len = len(args.data['src_tokens'])
    args.batch_count = int(np.ceil(args.train_len / args.batch_size))
    print('Number of examples :', args.train_len)

    sos_id = args.sos_id
    eos_id = args.eos_id
    pad_id = args.pad_id

    if args.relative_positioning is True:
        args.src_ids_list = [[sos_id] + [args.src_t2id[t] for t in s] + [eos_id]
                              for s in args.data['src_tokens']]
    else:
        args.src_ids_list = [[args.src_t2id[t] for t in s]
                              for s in args.data['src_tokens']]

    args.trg_ids_list = [[sos_id] + [args.src_t2id[t] for t in s] + [eos_id]
                         for s in args.data['trg_tokens']]
    # print('src ids', args.src_ids_list[:10])
    # print('trg ids', args.trg_ids_list[:10])

    src_counts = np.array([len(s) for s in args.src_ids_list])
    trg_counts = np.array([len(s) for s in args.trg_ids_list])

    # mean_counts = np.vstack([src_counts, trg_counts]).mean(axis=0)
    # mean_counts = src_counts

    args.src_max = np.max(src_counts)
    args.src_min = np.min(src_counts)
    args.trg_max = np.max(trg_counts)
    args.trg_min = np.min(trg_counts)
    print('Max source sequence length (with <eos> token):', args.src_max)
    print('Min source sequence length (with <eos> token):', args.src_min)
    print('Max target sequence length (with <sos> and <eos> tokens):',
          args.trg_max)
    print('Min target sequence length (with <sos> and <eos> tokens) :',
          args.trg_min)
    args.max_tokens = max(args.src_max, args.trg_max)
    args.src_mat = np.full((args.train_len, args.src_max), args.pad_id)
    args.trg_mat = np.full((args.train_len, args.trg_max), args.pad_id)
    # order samples from longest source length to shortest depending on source
    sort_ids_desc = np.argsort(src_counts)[::-1]

    for i in range(args.train_len):
        id_ = sort_ids_desc[i]
        args.src_mat[i, :len(args.src_ids_list[id_])] = args.src_ids_list[id_]
        args.trg_mat[i, :len(args.trg_ids_list[id_])] = args.trg_ids_list[id_]

    src_str_temp_list = [args.data['src_str_list'][i] for i in sort_ids_desc]
    args.data['src_str_list'] = src_str_temp_list

    src_tokens_temp_list = [args.data['src_tokens'][i] for i in sort_ids_desc]
    args.data['src_tokens'] = src_tokens_temp_list

    src_counts_temp_list = [src_counts[i] for i in sort_ids_desc]
    src_counts = np.array(src_counts_temp_list)
    trg_counts_temp_list = [trg_counts[i] for i in sort_ids_desc]
    trg_counts = np.array(trg_counts_temp_list)

    src_ids_temp_list = [args.src_ids_list[i] for i in sort_ids_desc]
    args.src_ids_list = np.array(src_ids_temp_list, dtype='object')
    trg_ids_temp_list = [args.trg_ids_list[i] for i in sort_ids_desc]
    args.trg_ids_list = np.array(trg_ids_temp_list, dtype='object')

    train_eval_mask = np.random.rand(args.train_len) > args.eval_ratio

    train_iterator = DataIterator(args.src_mat[train_eval_mask],
                                  args.trg_mat[train_eval_mask],
                                  src_counts[train_eval_mask],
                                  trg_counts[train_eval_mask],
                                  args.batch_size,
                                  args.src_vocab_size,
                                  args.tokens_frequency, args.device,
                                  args.batch_cuts)

    eval_iterator = DataIterator(args.src_mat[~train_eval_mask],
                                 args.trg_mat[~train_eval_mask],
                                 src_counts[~train_eval_mask],
                                 trg_counts[~train_eval_mask],
                                 args.batch_size,
                                 args.src_vocab_size,
                                 args.tokens_frequency, args.device,
                                 args.batch_cuts)

    return train_iterator, eval_iterator, args


class DataIterator:
    def __init__(self, src_mat, trg_mat, src_counts, trg_counts,
                 batch_size, src_vocab_size, tokens_frequency,
                 device, batch_cuts):
        self.src_mat = src_mat
        self.trg_mat = trg_mat
        self.src_counts = src_counts
        self.trg_counts = trg_counts
        self.batch_size = batch_size
        self.src_vocab_size = src_vocab_size
        self.tokens_frequency = tokens_frequency
        self.device = device
        self.examples_count = src_mat.shape[0]
        self.batch_count = int(np.ceil(self.examples_count / self.batch_size))
        self.current = -1
        self.processed = 0
        self.nb_cuts = batch_cuts
        self.ordered_batches = None
        self.order_batches()

    def order_batches(self):
        nb_cuts = self.nb_cuts
        nb_batch = self.batch_count
        batch_indexes = []
        for i in range(nb_batch):
            start = i*self.batch_size
            end = min(start+self.batch_size, self.examples_count)
            batch_indexes.append((start, end))
        cut_size = math.ceil(nb_batch / nb_cuts)
        cuts = [None] * nb_cuts
        for i in range(nb_cuts):
            start = i * cut_size
            end = min(start + cut_size, nb_batch)
            cuts[i] = batch_indexes[start:end]
            if i % 2 == 1:
                cuts[i] = cuts[i][::-1]
        interleaved = []
        for i in range(nb_cuts):
            index = 0 if i % 2 == 0 else -1
            interleaved.append(cuts.pop(index))
        self.ordered_batches = []
        nb_proccessed = 0
        while nb_proccessed < nb_batch:
            i = nb_proccessed % len(interleaved)
            try:
                self.ordered_batches.append(interleaved[i].pop(0))
                nb_proccessed += 1
            except IndexError:
                interleaved.pop(i)
                pass
        # show lengths histogram then batch lengths ordering for each epoch
        # first = [self.src_counts[t[0]] for t in self.ordered_batches]
        # plt.figure()
        # plt.plot(range(len(self.src_counts)), self.src_counts)
        # plt.show()
        # plt.figure()
        # plt.plot(range(len(first)), first)
        # plt.show()

    def __iter__(self):
        return self

    def __next__(self):
        self.current += 1
        if self.processed < self.examples_count:
            start, end = self.ordered_batches[self.current]
            batch_size = end - start
            src_max_len = np.max(self.src_counts[start:end])
            trg_max_len = np.max(self.trg_counts[start:end])
            batch_mat_src = from_numpy(self.src_mat[start:end, :src_max_len]).long()
            batch_mat_trg = from_numpy(self.trg_mat[start:end, :trg_max_len]).long()
            self.processed += batch_size
            return (batch_mat_src.to(self.device),
                    batch_mat_trg.to(self.device),
                    self.src_counts[start:end],
                    start, end)
        else:
            self.current = -1
            self.processed = 0
            raise StopIteration
