# coding: utf-8
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.autograd import Variable
from torch.nn import Identity
from src.layers import EncoderLayer, DecoderLayer
from src.embed import Embedder, PositionalEncoder, RoFormerSinusoidalPositionalEmbedding
from src.sublayers import FeedForward, MultiHeadAttention, Norm
import os
import copy


def get_clones(module, N):
    return nn.ModuleList([copy.deepcopy(module) for i in range(N)])


class Encoder(nn.Module):
    def __init__(self, embed, pe, vocab_size, d_vocab,
                 d_hidden, N, tied_layers, heads,
                 d_ff, ape_style, rpe, rpe_style, fixed,
                 rot_pe, ff_activation, max_seq_len, device):
        super().__init__()
        self.N = N
        self.embed = embed
        self.pe = pe
        self.ape_style = ape_style
        self.rot_pe = rot_pe
        if tied_layers is True:
            encoder = EncoderLayer(d_hidden, heads, d_ff,
                                   rpe, rpe_style, fixed,
                                   ff_activation, max_seq_len,
                                   device)
            self.layers = [encoder] * N
        else:
            encoder = EncoderLayer(d_hidden, heads, d_ff,
                                   rpe, rpe_style, fixed,
                                   ff_activation, max_seq_len,
                                   device)
            self.layers = get_clones(encoder, N)
        self.norm = Norm(d_hidden, device)

    def forward(self, src, mask):
        x = self.embed(src)
        if self.rot_pe is not None:
            # [sequence_length, embed_size_per_head] -> [batch_size, num_heads, sequence_length, embed_size_per_head]
            rot_pos = self.rot_pe(x.shape[:-1])[None, None, :, :]
        if self.ape_style == 'before':
            x = self.pe(x)
        for i in range(self.N):
            x = self.layers[i](x, mask, rot_pos)
        if self.ape_style == 'after':
            x = self.pe(x)
        return self.norm(x)


class Decoder(nn.Module):
    def __init__(self, embed, pe, vocab_size, d_vocab,
                 d_hidden, N, tied_layers, heads,
                 d_ff, ape_style, rpe, rpe_style, fixed,
                 rot_pe, ff_activation, max_seq_len, device):
        super().__init__()
        self.N = N
        self.embed = embed
        self.pe = pe
        self.ape_style = ape_style
        self.rot_pe = rot_pe
        if tied_layers is True:
            decoder = DecoderLayer(d_hidden, heads, d_ff,
                                   rpe, rpe_style, fixed,
                                   ff_activation, max_seq_len,
                                   device)
            self.layers = [decoder] * N
        else:
            decoder = DecoderLayer(d_hidden, heads, d_ff,
                                   rpe, rpe_style, fixed,
                                   ff_activation, max_seq_len,
                                   device)
            self.layers = get_clones(decoder, N)
        self.norm = Norm(d_hidden, device)

    def forward(self, trg, e_outputs, src_mask, trg_mask):
        x = self.embed(trg)
        if self.rot_pe is not None:
            # [sequence_length, embed_size_per_head] -> [batch_size, num_heads, sequence_length, embed_size_per_head]
            rot_pos = self.rot_pe(x.shape[:-1])[None, None, :, :]
        if self.ape_style == 'before':
            x = self.pe(x)
        for i in range(self.N):
            x = self.layers[i](x, e_outputs, src_mask, trg_mask, rot_pos)
        if self.ape_style == 'after':
            x = self.pe(x)
        return self.norm(x)


class NMT_Transformer(nn.Module):
    def __init__(self, vocab_size, d_vocab, d_hidden,
                 Ne, Nd, tied_layers, ape, ape_style, ape_enc,
                 rpe, rpe_style, fixed, rot_pe, ff_activation,
                 heads, d_ff, dropout, max_seq_len, pad_id, device):
        super().__init__()
        if ape is True:
            self.pe = PositionalEncoder(d_hidden, fixed, device, ape_enc,
                                        max_seq_len=max_seq_len)
        else:
            self.pe = Identity()
        if rot_pe is True:
            self.rot_pe = RoFormerSinusoidalPositionalEmbedding(
                max_seq_len, d_hidden // heads
            )
        else:
            self.rot_pe = None
        self.embed = Embedder(vocab_size, d_vocab, d_hidden,
                              max_seq_len, pad_id, device)
        self.encoder = Encoder(self.embed, self.pe, vocab_size, d_vocab,
                               d_hidden, Ne, tied_layers, heads, d_ff,
                               ape_style, rpe, rpe_style, fixed, self.rot_pe,
                               ff_activation,
                               max_seq_len, device)
        self.decoder = Decoder(self.embed, self.pe, vocab_size, d_vocab,
                               d_hidden, Nd, tied_layers, heads, d_ff,
                               ape_style, rpe, rpe_style, fixed, self.rot_pe,
                               ff_activation,
                               max_seq_len, device)
        if d_vocab == d_hidden:
            self.to_vocab = Identity()
        else:
            self.to_vocab = nn.Linear(d_hidden, d_vocab, bias=True)
        self.to_token = nn.Linear(d_vocab, vocab_size, bias=True)

    def forward(self, src, trg, src_mask, trg_mask):
        e_outputs = self.encoder(src, src_mask)
        d_output = self.decoder(trg, e_outputs, src_mask, trg_mask)
        out_vocab = self.to_vocab(d_output)
        out_token = self.to_token(out_vocab)
        # out_vocab = F.linear(d_output, self.embed.hidden_embed.weight.data.t())
        # out_token = F.linear(out_vocab, self.embed.vocab_embed.weight.data)
        return out_token


def get_model(args):
    assert args.d_hidden % args.heads == 0
    assert 0.0 <= args.dropout < 1.0

    model = NMT_Transformer(args.src_vocab_size, args.d_vocab, args.d_hidden,
                            args.n_encoder_layers, args.n_decoder_layers,
                            args.tied_layers, args.absolute_positioning,
                            args.absolute_positioning_style,
                            args.absolute_positioning_encoding,
                            args.relative_positioning,
                            args.relative_positioning_style,
                            args.fixed_positioning,
                            args.rotary_positioning,
                            args.ff_activation,
                            args.heads, args.d_ff,
                            args.dropout, args.max_tokens, args.pad_id, args.device)

    model_file = args.ckpt_file or 'model_weights.ckpt'
    model_path = os.path.join('trainings', args.load_weights, model_file)
    if args.load_weights is not None \
            and os.path.exists(model_path):
        print("loading pretrained weights from", model_path)
        model.load_state_dict(torch.load(model_path))
    else:
        for p in model.parameters():
            if p.dim() > 1:
                nn.init.xavier_uniform_(p)
    if args.device == 0:
        model = model.to(args.device)
    return model


def nopeak_mask(size, args):
    np_mask = np.triu(np.ones((1, size, size)),
                      k=1).astype('uint8')
    np_mask = Variable(torch.from_numpy(np_mask) == 0)
    # np_mask.size = 1 * seq_len+1 * seq_len+1
    if args.device == 0:
        np_mask = np_mask.to(args.device)
    return np_mask


def create_masks(src, trg, args):
    src_mask = (src != args.pad_id).unsqueeze(-2)
    if trg is not None:
        trg_mask = (trg != args.pad_id).unsqueeze(-2)
        size = trg.size(1)  # get seq_len for matrix
        np_mask = nopeak_mask(size, args)
        trg_mask = trg_mask & np_mask
    else:
        trg_mask = None
    # src_mask & trg_mask dimensions are batch_size * 1 * seq_len
    return src_mask, trg_mask
