import numpy as np
import math
import matplotlib.pyplot as plt

max_length = 128
nb_samples = 20000
batch_size = 96
nb_cuts = 16

samples = np.random.randint(0, max_length, nb_samples)

argsort = np.argsort(samples)[::-1]
samples_sorted = [samples[i] for i in argsort]

nb_batch = math.ceil(nb_samples / batch_size)
print('nb_batch', nb_batch)

batch_indexes = []
for i in range(nb_batch):
    start = i*batch_size
    end = min(start+batch_size, nb_samples)
    batch_indexes.append((start, end))

print('len(batch_indexes)', len(batch_indexes))
print('batch_indexes', batch_indexes)

cut_size = math.ceil(nb_batch / nb_cuts)
cuts = [None] * nb_cuts
for i in range(nb_cuts):
    start = i * cut_size
    end = min(start + cut_size, nb_batch)
    print('start', start, 'end', end)
    cuts[i] = batch_indexes[start:end]
    if i % 2 == 1:
        cuts[i] = cuts[i][::-1]
interleaved = []
for i in range(nb_cuts):
    index = 0 if i%2 == 0 else -1
    interleaved.append(cuts.pop(index))
print('len(interleaved)', len(interleaved))
for i in range(len(interleaved)):
    print('len(interleaved[i])', len(interleaved[i]))
print('interleaved', interleaved)

ordered_batches = []

nb_proccessed = 0
while nb_proccessed < nb_batch:
    i = nb_proccessed % nb_cuts
    print('i', i, 'nb_proccessed', nb_proccessed, '/', nb_batch)
    try:
        ordered_batches.append(interleaved[i].pop(0))
    except IndexError:
        pass
    nb_proccessed += 1

print('ordered_batches', ordered_batches)

means = [np.mean(t) for t in ordered_batches]
first = [samples_sorted[t[0]] for t in ordered_batches]


plt.figure()
plt.plot(range(len(samples_sorted)), samples_sorted)
plt.show()

plt.figure()
plt.plot(range(len(first)), first)
plt.show()
