from dateutil import rrule
from datetime import datetime
import os
from tqdm import tqdm


def ord(n):
    return str(n)+("th" if 4<=n%100<=20 else {1:"st",2:"nd",3:"rd"}.get(n%10, "th"))


def dtStylish(dt, f):
    return dt.strftime(f).replace("{th}", ord(dt.day))


a = '1760-01-01'
b = '2080-12-31'

lines = []

for dt in tqdm(rrule.rrule(rrule.DAILY,
                           dtstart=datetime.strptime(a, '%Y-%m-%d'),
                           until=datetime.strptime(b, '%Y-%m-%d')),
               desc='Processing dates'):
    date = dt.strftime('%Y-%m-%d')
    fdate = dtStylish(dt, '%A, %B the {th}, %Y')
    # print(date)
    # print(fdate)
    lines.append(date+'\t'+fdate+'\n')

with open(os.path.join('data', 'dates_dataset_1760-2080.csv'), 'w', encoding='utf8') as f:
    f.writelines(lines)
